# ChatonsInfos Changelog

All notable changes of ChatonsInfos will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Added
### Changed
### Deprecated
### Removed
### Fixed


## [0.4] - 2021-12-16
### Added
- organization.geolocation.latitude : coordonnées GPS, latitude (type DECIMAL_DEGREE, recommandé, format DD, ex. 15,23456).
- organization.geolocation.longitude : coordonnées GPS, longitude (type DECIMAL_DEGREE, recommandé, format DD, ex. -30,67890).
- organization.geolocation.address : adresse de l'organisation (type STRING, recommandé, ex. 1 rue croquette, 92370 Chaville).
- support du fichier metrics.properties.
### Changed
- service.url : changement de « Recommandé » à « Obligatoire ».
### Deprecated
### Removed
### Fixed


## [0.3] - 2021-05-06
### Added
- organization.country.name : pays de l'organisation (type STRING, recommandé, ex. France).
- organization.country.code : code pays de l'organisation (type COUNTRY_CODE sur 2 caractères, obligatoire, ex.ex. FR ou BE ou CH ou DE ou GB). Table ISO 3166-1 alpha-2, https://fr.wikipedia.org/wiki/ISO_3166-1#Table_de_codage
- service.registration.load : capacité à accueillir de nouveaux utilisateurs (un parmi {open, full}, obligatoire).
### Changed
- organization.socialnetworks.* : ajout d'un commentaire explicitant le fonctionnement des champs concernant les comptes sur les réseaux sociaux des organisations.
- organization.chatrooms.* : ajout d'un commentaire explicitant le fonctionnement des champs concernant les salons de discussion publique des organisations.
- host.country.code : changement du type de STRING à COUNTRY_CODE (ISO 3166-1 alpha-2, https://fr.wikipedia.org/wiki/ISO_3166-1#Table_de_codage, ex. FR ou BE ou CH ou DE ou GB).
- host.country.code : changement de recommandé à obligatoire.
- service.startdate : changement de recommandé à obligatoire.

### Deprecated
### Removed
### Fixed


## [0.2] - 2021-01-11
### Added
- software.modules : liste de modules optionnels installés (type VALUES, optionnel, ex. Nextcloud-Calendar, Nextcloud-Talk).
- organization.chatrooms.foo : lien du salon public foo de l'organisation (type STRING, optionnel).
- organization.memberof.chatons.startdate : date d'entrée dans le collectif (type DATE, obligatoire, ex. 08/11/2018).
- organization.memberof.chatons.enddate : date de sortie du collectif (type DATE, optionnel, ex. 08/11/2019).
- organization.memberof.chatons.status.level : statut en tant que membre de l'organisation (un parmi {ACTIVE, IDLE, AWAY}, obligatoire).
- organization.memberof.chatons.status.description : description du statut en tant que membre de l'organisation (type STRING, optionnel, ex. en sommeil).
- service.install.type : type d'installation du service, une valeur parmi {DISTRIBUTION, PROVIDER, PACKAGE, CLONEREPO, ARCHIVE, SOURCES, CONTAINER}, obligatoire.

### Changed
- host.description : recommandé -> optionnel.
- federation.socialnetworks.*: recommandé -> optionnel.
- organization.socialnetworks.*: recommandé -> optionnel.
### Deprecated
### Removed
### Fixed


## [0.1] - 2020-11-23
### Added
- all
### Changed
### Deprecated
### Removed
### Fixed

